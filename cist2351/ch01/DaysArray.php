<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <title>Days Array</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Riaz Virani" />
    </head>
    <body>
        <?php
            $Days = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            echo "The days of the week in English are: ", $Days[0], ", ", $Days[1], ", ", $Days[2], ", ", $Days[3], ", ", $Days[4], ", ", $Days[5], ", ", $Days[6], "<br />";
            $Days[0] = "Dimanche";
            $Days[1] = "Lundi";
            $Days[2] = "Mardi";
            $Days[3] = "Mercredi";
            $Days[4] = "Jeudi";
            $Days[5] = "Vendredi";
            $Days[6] = "Samedi";
            echo "The days of the week in French are: ", $Days[0], ", ", $Days[1], ", ", $Days[2], ", ", $Days[3], ", ", $Days[4], ", ", $Days[5], ", ", $Days[6];
        ?>

    </body>

</html>
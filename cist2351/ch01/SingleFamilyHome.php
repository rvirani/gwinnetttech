<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <title>Single Family Home</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Riaz Virani" />
    </head>
    <body>
        <?php
            $SingleFamilyHome = 399500;
            $SingleFamilyHome_Display = number_format($SingleFamilyHome, 2);
            echo "<p>The current median price of a single family home in Pleasanton, CA is $$SingleFamilyHome_Display.</p>";

        ?>

    </body>

</html>
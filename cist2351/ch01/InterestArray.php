<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <title>Interest Array</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Riaz Virani" />
    </head>
    <body>
        <?php
            $RatesArray = array();
            $InterestRate1 = 0.0725;
            $RatesArray[] = $InterestRate1;
            $InterestRate2 = 0.0750;
            $RatesArray[] = $InterestRate2;
            $InterestRate3 = 0.0775;
            $RatesArray[] = $InterestRate3;
            $InterestRate4 = 0.0800;
            $RatesArray[] = $InterestRate4;
            $InterestRate5 = 0.0825;
            $RatesArray[] = $InterestRate5;
            $InterestRate6 = 0.0850;
            $RatesArray[] = $InterestRate6;
            $InterestRate7 = 0.0875;
            $RatesArray[] = $InterestRate7;

            echo "<pre>";
            print_r($RatesArray);
            echo "</pre>";

        ?>

    </body>

</html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <title>Days Array</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Riaz Virani" />
    </head>
    <body>
        <?php
            $Value1 = 4.3;
            $Value2 = 2.6;
            echo "This is 4.3 rounded down to 4: ", round($Value1), "<br />";
            echo "This is 4.3 with a ceiling up to 5: ", ceil($Value1), "<br />";
            echo "This is 2.6 with a floor down to 2: ", floor($Value2), "<br />";
        ?>

    </body>

</html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <title>Is Even</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="author" content="Riaz Virani" />
    </head>
    <body>
        <?php
            $Value = 3;
            echo (is_numeric($Value) && ((round($Value) % 2) == 0)
            	? "True"
            	: "False");
        ?>

    </body>

</html>
<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Chapter 3 Lab</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Chapter 3 Lab</h1><hr />
<?php
    $firstString = "Now is the time";
    $secondString = "for all good citizens";
    $thirdString = "to come to the aid of their country.";
    $combinedString = $firstString . $secondString . $thirdString;
    $originalString = "Now is the time for all good men to come to the aid of the party.";
    if (strcasecmp($combinedString, $originalString) == 0) {
        echo "The concatenated quote is the same as the original quote";

    } else {
        echo "The strings are not the same";
    }
?>
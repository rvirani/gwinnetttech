<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Compare Strings</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Compare Strings</h1><hr />
<?php
    $firstString = "Geek2Geek";
    $secondString = "Geezer2Geek";
    if (!empty($firstString) && !empty($secondString)) {
        if ($firstString == $secondString)
            echo "<p>Both strings are the same.</p>";
        else {
            echo "<p>Both strings have " . similar_text($firstString, $secondString) . " characters(s) in common.<br />";
            echo "<p>You must change " . levenshtein($firstString, $secondString) . " character(s) to make the strings the same.<br />";
        }
    } else {
        echo "<p>Either the \$firstString variable or the \$secondString variable does not contain a value so the two strings cannot be compared. </p>";
    }
?>
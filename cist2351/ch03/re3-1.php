<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Validate Credit Card</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Validate Credit Card</h1><hr />
<?php
    $CreditCard = array(
        "",
        "8910-1234-5678-6543",
        "0000-9123-4567-0123",
        "234 2342 -23432",
        "12343aafd");
    foreach ($CreditCard as $CardNumber) {
        if (empty($CardNumber)) {
            echo "<p>This Credit Card Number is invalid because it contains an empty string.</p>";
        } else {
            $CreditCardNumber = $CardNumber;
            $CreditCardNumber = str_replace("-", "", $CreditCardNumber);
            $CreditCardNumber = str_replace(" ", "", $CreditCardNumber);
            if (!is_numeric($CreditCardNumber)) {
                echo "<p>Credit Card Number " .$CreditCardNumber ." is not a valid Credit Card number because it contains a non-numeric character. </p>";
            } else
                echo "</>Credit Card Number " .$CreditCardNumber ." is a valid Credit Card number.</p>";
        }
    }
?>
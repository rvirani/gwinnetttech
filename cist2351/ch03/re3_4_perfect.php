<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Perfect Palindrome</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Perfect Palindrome</h1><hr />
<?php
    $perfectString = "racecar";
    if (strcasecmp($perfectString, strrev($perfectString)) == 0)
        echo "This string is a perfect palindrome";
    else
        echo "This string is not a perfect palindrome";
?>
<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Standard Palindrome</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Standard Palindrome</h1><hr />
<?php
    $standardString = "Madam, ";
    $whatToStrip = array("?","!",",",";"," ","'",";",":","<",">","/","-","_");
    $standardString = str_replace($whatToStrip, "", $standardString);
    if (strcasecmp($standardString, strrev($standardString)) == 0)
        echo "This string is a standard palindrome";
    else
        echo "This string is not a standard palindrome";
?>
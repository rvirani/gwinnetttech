<!--
* Created by PhpStorm.
* User: Riaz Virani
*-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Passwords</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Passwords</h1><hr />
<?php
    function debug_to_console( $data ) {
        if ( is_array( $data ) )
            $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
        else
            $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
        echo $output;
    }

$Passwords = array("1GoodPassword.","FCKOSFMN393!","7vphakfdp1bl!","u3JD 4bSCd7cy!","PFQDw4tQbD1i","4bIHHH9aWUWm!","1sYXEAp01dja!","Y5mYhSWaUOPj!","rM9SD4VSJdmK!","So7brHVoqZjj!");
$Passwords2 = array("NoDigitsOrOthers",
    "NoDigits...",
    "NoOthers999",
    "2 Spaces !",
    "0LOWERCASE?",
    "0uppercase+",
    "Sh0r+",
    "This1IsMuchTooLongForAPassword!",
    "1GoodPassword.",
    "GoodPassword2!");

    foreach($Passwords as $Password) {
        $Failed = False;
        if (preg_match("/\d/",$Password)=== 0) {
            $Failed = True;
            debug_to_console("check 1");
        }


        if (preg_match("/[a-z]/",$Password) === 0) {
            $Failed = True;
            debug_to_console("check 2");
        }

        if (preg_match("/[A-Z]/",$Password) === 0) {
            $Failed = True;
            debug_to_console("check 3");
        }

        if (preg_match("/\s/",$Password) === 1) {
            $Failed = True;
            debug_to_console("check 4");
        }

        if (preg_match("/[^A-Za-z\d]/",$Password) === 0) {
            $Failed = True;
            debug_to_console("check 5");
        }

        if ((strlen($Password) > 16) || (strlen($Password) < 8)) {
            $Failed = True;
            debug_to_console("check 6");
        }

        if ($Failed === False) {
            echo $Password . " is a strong password. <br />";
        } else {
            echo $Password . " is not a strong password. <br />";
        }
    }

?>
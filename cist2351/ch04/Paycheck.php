<?php
/**
 * Created by PhpStorm.
 * User: e6420
 * Date: 2/24/14
 * Time: 10:12 PM
 */

 function displayError($fieldName, $errorMsg) {
    global $errorCount;
    echo "Error for {$fieldName}: $errorMsg<br />\n";
    ++$errorCount;
}

 function validate($data, $fieldName) {

     if (empty($data)) {
         displayError($fieldName, "This field is required");
     } else {
         if (!(is_numeric($data))) {
             displayError($fieldName, "Values can only be numbers");
         }
         if ((preg_match('/\.\d{3,}/', $data))) {
             displayError($fieldName, "Values can only have two decimal places");
         }

     }
     return ($data);
 }

 $errorCount = 0;
 $hoursWorked = validate($_POST['hoursWorked'], "Hours Worked");
 $hourlyWage =  validate($_POST['hourlyWage'], "Hourly Wage");
 $total = $hoursWorked * $hourlyWage;

if ($errorCount > 0) {
    echo "Please use the \"Back\" button to re-enter the data.<br />\n";
} else {
    echo "You worked " . $hoursWorked . " hours and earned $" . $total . ".<br />\n";
}
?>
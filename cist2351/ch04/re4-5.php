<!DOCTYPE html>
<html>
<head>
    <title>RE4-5</title>
</head>
<body>
<?php
    $DisplayForm = TRUE;
    $SpeedA = "";
    $SpeedB = "";
    $Distance = "";
    $error = FALSE;
    if (isset($_POST['Submit'])) {
        $SpeedA = $_POST['speeda'];
        $SpeedB = $_POST['speedb'];
        $Distance = $_POST['distance'];
        if (($SpeedA < 0) or !(is_numeric($SpeedA))) {
            echo "<p>You need to enter a positive numeric value for the speed of train A.</p>\n";
            $error = TRUE;
        }
        if (($SpeedB < 0) or !(is_numeric($SpeedB))) {
            echo "<p>You need to enter a positive numeric value for the speed of train B.</p>\n";
            $error = TRUE;
        }
        if (($Distance < 0) or !(is_numeric($Distance))) {
            echo "<p>You need to enter a positive numeric value for distance.</p>\n";
            $error = TRUE;
        }
        if (!$error) {
            $DisplayForm = FALSE;
        }
    }
if ($DisplayForm == TRUE) {
    ?>
    <form name="re45" action="re4-5.php" method="post">
        <p>Enter the speed of train A: <input type="text" name="speeda" value="<?php echo $SpeedA; ?>" /></p>
        <p>Enter the speed of train B: <input type="text" name="speedb" value="<?php echo $SpeedB; ?>" /></p>
        <p>Enter the distance between the trains: <input type="text" name="distance" value="<?php echo $Distance; ?>" /></p>
        <p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="Submit" value="Send Form" /></p>
    </form>
<?php
} else {
    $DistanceA = (($SpeedA / $SpeedB) * $Distance) / (1 + ($SpeedA / $SpeedB));
    $DistanceB = $Distance - $DistanceA;
    $TimeA = $DistanceA / $SpeedA;
    $TimeB = $DistanceB / $SpeedB;
    echo "<p>The distance traveled by train A is  ". $DistanceA .".</p>\n ";
    echo "<p>The distance traveled by train B is  ". $DistanceB .".</p>\n ";
    echo "<p>The time for train A is ". $TimeA .".</p>\n ";
    echo "<p>The time for train B is ". $TimeB .".</p>\n ";
    echo "<p><a href='re4-5.php'>Try again?</a></p>\n";
    }
?>
</body>
</html>
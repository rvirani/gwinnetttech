<!DOCTYPE html>
<html>
<head>
    <title>Remove Values</title>
</head>
<body>
<?php
    $DisplayForm = TRUE;
    $data = "";
    if (isset($_POST['Submit'])) {
        $data = $_POST['data'];
        if (strlen($data) > 0) {
            $DisplayForm = FALSE;
        } else {
            echo "<p>You need to enter a value.</p>\n";
            $DisplayForm = TRUE;
        }

    }
if ($DisplayForm == TRUE) {
    ?>
    <form name="ch04lab" action="ch04lab.php" method="post">
        <p>Enter a value: <input type="text" name="data" value="<?php echo $data; ?>" /></p>
        <p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="Submit" value="Send Form" /></p>
    </form>
<?php
} else {
    echo "<p>Thank You for entering a value.</p>\n";
    $data = str_replace("<","",$data);
    $data = str_replace(">","",$data);
    echo "<p>Your value without brackets is ". $data .".</p>\n ";
    echo "<p><a href='ch04lab.php'>Try again?</a></p>\n";
    }
?>
</body>
</html>